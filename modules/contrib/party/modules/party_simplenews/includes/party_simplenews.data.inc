<?php
/**
 * @file
 * Provides the class for managing subscription settings data sets.
 */

/**
 * Controller class for CRM integration.
 */
 class PartySubscriptionSettingsDataSet extends PartyDefaultDataSet {
  public function getLabel() {
    return 'Newsletters';
  }
}
 