<?php

/**
 * @file
 * Contains install hooks for the CRM party module.
 */

/**
 * Implements hook_schema().
 */
function party_schema() {
  $schema['party'] = array(
    'description' => 'Stores Id, name and email for party contacts',
    'fields' => array(
      'pid' => array(
        'description' => 'Primary key for the party.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'label' => array(
        'description' => 'A label for the party. This is generated by one of a number of plugins.',
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        'default' => 'No Label Yet',
      ),
      'merged' => array(
        'description' => 'Boolean flag for whether this party has been merged with another',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'merged_party' => array(
        'description' => 'The pid of the party this has been merged into',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
    ) + party_exportable_schema_fields(),
    'primary key' => array('pid'),
  );

  $schema['party_party_pieces'] = array(
    'description' => 'Stores relationships between party (types) and party pieces',
    'fields' => array(
      'instance_id' => array(
        'description' => 'Primary key for the party piece instance.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'parent_id' => array(
        'description' => 'The id of the parent party piece instance.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'title' => array(
        'description' => 'The title of this party piece',
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE
      ),
      'party_type' => array(
        'description' => 'The party type.',
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        'default' => '',
      ),
      'party_piece' => array(
        'description' => 'The party piece type.',
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        'default' => '',
      ),
      'arguments' => array(
        'description' => 'Any extra filters or arguments the party piece should apply. In json format.',
        'type' => 'text',
        'not null' => true,
      ),
      'weight' => array(
        'description' => 'The ordering number of the party piece',
        'type' => 'int',
        'not null' => true,
        'default' => 0,
      ),
    ),
    'primary key' => array('instance_id'),
  );


  // Connect parties to attached entities.
  $schema['party_attached_entity'] = array(
    'description' => "This links parties to attached entities.",
    'fields' => array(
      'delta' => array(
        'description' => 'The delta of this attached entity, so that every record in the table is unique',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'pid' => array(
        'description' => 'The id of the party entity.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'eid' => array(
        'description' => 'The id of the attached entity.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'data_set' => array(
        'description' => 'The data set this entity falls under.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ),
      'entity_type' => array(
        'description' => 'The type of the attached entity.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ),
      'entity_bundle' => array(
        'description' => 'The bundle of the attached entity.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ),
      'hat_main' => array(
        'description' => 'Is this the main set for this hat.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'hat' => array(
        'description' => 'The hat this is associated with.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => 'all',
      ),
    ),
    'indexes' => array(
      'party' => array('pid'),
      'entity' => array('eid'),
    ),
    'primary key' => array('pid', 'data_set', 'delta'),
  );

  return $schema;
}

/**
 * Exportable schema fields.
 */
function party_exportable_schema_fields() {
  return array(
    'status' => array(
      'type' => 'int',
      'not null' => TRUE,
      // Set the default to ENTITY_CUSTOM without using the constant as it is
      // not safe to use it at this point.
      'default' => 0x01,
      'size' => 'tiny',
      'description' => 'The exportable status of the entity.',
    ),
    'module' => array(
      'description' => 'The name of the providing module if the entity has been defined in code.',
      'type' => 'varchar',
      'length' => 255,
      'not null' => FALSE,
    ),
  );
}

/**
 * Add a default value to party labels.
 */
function party_update_7021() {
  db_change_field('party', 'label', 'label', array(
      'description' => 'A label for the party. This is generated by one of a number of plugins.',
      'type' => 'varchar',
      'length' => '255',
      'not null' => TRUE,
      'default' => 'No Label Yet',
    )
  );
}

/**
 * Drop Party Bundles.
 */
function party_update_7022() {
  db_drop_field('party', 'type');
}
