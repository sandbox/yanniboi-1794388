<?php

/**
 * @file
 * Tests for the Party module.
 */

/**
 * Base class for all Party tests.
 */
class PartyBaseTestCase extends DrupalWebTestCase {

  /**
   * Create a test profile2 type and return the profile2 type machine name.
   */
  public function createTestProfile2() {
    // Create a Party Test Profile2 type.
    $random_name = $this->randomName(8);
    $label = 'Party Test ' . $random_name;
    $machine_name = 'party_test_' . strtolower($random_name);
    $profile_type = new ProfileType(array(
            'type' => $machine_name,
            'label' => $label,
            'weight' => 0
    ));
    profile2_type_save($profile_type);

    // Clear Profile2 cache.
    profile2_load_multiple(FALSE, array(), TRUE);
    // Clear the Data Set cache.
    $this->assertNotNull(party_get_data_set_info(NULL, TRUE));

    // Add a field to Party Test type.
    $field = array(
            'field_name' => 'party_test_field',
            'type' => 'text',
            'cardinality' => 1,
            'translatable' => FALSE,
    );
    field_create_field($field);
    $instance = array(
            'entity_type' => 'profile2',
            'field_name' => 'party_test_field',
            'bundle' => $machine_name,
            'label' => 'Party Test Field',
            'description' => 'Party Test Field - Text type.',
            'widget' => array(
                    'type' => 'text_textfield',
                    'weight' => 0,
            ),
    );
    field_create_instance($instance);

    // @todo: Check permission have been created for the new data set.
    //$this->checkPermissions(array('attach party profile2_' . $machine_name), TRUE);

    return $machine_name;
  }

}

/**
 * Test Core Party functionality
 */
class PartyTestCase extends PartyBaseTestCase {
  protected $privileged_user;

  public static function getInfo() {
    return array(
            'name' => 'Party',
            'description' => 'Party CRUD test.',
            'group' => 'Party',
    );
  }

  public function setUp() {
    parent::setUp('party');  // Enable any modules required for the test
    $this->privileged_user = $this->drupalCreateUser(array(
            'administer crm settings',
            'administer parties',
            'create parties',
            'view parties',
    ));
    $this->drupalLogin($this->privileged_user);
  }

  /**
   * Test CRUD on the Party Entities
   */
  public function testPartyCRUD() {
    // Test Party Creation.
    $this->drupalGet('admin/community');
    $this->clickLink('Add party');
    // Assert that the add party page exists and is accessible.
    $this->assertRaw(t('Add party'), t('Add Party page found'));
    $this->drupalPost(null, array(), t('Save'));

    // Test Reading Parties.
    $this->drupalGet('admin/community');
    // Assert that the created party appears on the Party List page.
    $this->assertRaw('Party 1', t('Test Party found'));
    // Assert that there is a party with pid 1 in the database.
    $this->assertNotNull(db_query("SELECT pid FROM {party} WHERE pid = 1"), 'Party with pid = 1 found in db');

    // Test Updating Parties.
    // @TODO Need to add field to main profile and make sure it works with Party?
    // @TODO Also change the Party label?

    // Test Party Deletion.
    $this->drupalGet('admin/community');
    $this->clickLink('delete');
    $this->drupalPost(null, array(), t('Confirm'));
    $this->drupalGet('admin/community');
    // Assert that Party no longer appears on the Party List page (admin/community).
    $this->assertNoRaw('Party 1', t('Test Party not found (delete worked)'));

    ///////////////////////////////////////////////////////////////////////////
    //                   Party CRUD Programmatically
    ///////////////////////////////////////////////////////////////////////////

    // Test Party Creation.
    $party = party_create();
    $party->save();
    // Assert the Party Object was Created and Saved.
    $this->assertNotNull($party);
    // Assert that a pid has been set.
    $this->assertNotNull($party->pid);
    // Assert that the Party Label has been generated correctly.
    $this->assertEqual($party->label, 'Party ' . $party->pid);

    // Test Reading Parties.
    $pid = $party->pid;
    unset($party);
    $party = party_load($pid);
    // Assert that party_load has returned an object (and not FALSE).
    $this->assertTrue(is_object($party));
    // Asset that correct party has been loaded.
    $this->assertEqual($pid, $party->pid);

    // Test Updating Parties
    // @TODO Need to do some sort of update.

    // Test Party Deletion
    party_delete($party);
    // Assert that the Party can no longer be loaded.
    $this->assertFalse(party_load($pid));

    // Make sure there is no party with the deleted party's pid in database.
    $result = db_query("SELECT pid FROM {party} WHERE pid = :pid", array(':pid'=>$pid))->fetchField();
    // Assert that no Party was found with this pid.
    $this->assertFalse($result, 'Party with pid = '. $pid . ' not found in db (delete worked)');
  }
}
